# Custom Bootstrap
Simple demonstration of how to generate a custom bootstrap CSS.

```bash
npm install
npm run build
```

This will compile the sass file, `bootstrap.scss`, into a new CSS file.
